const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const UglifyPlugin = require('uglifyjs-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.jsx',
  output: {
    path: path.resolve('dist'),
    filename: 'index.js',
  },
  module: {
    rules: [{
      test: /.jsx?$/,
      exclude: path.resolve('node_modules'),
      use: {
        loader: 'babel-loader',
      },
    }],
  },
  plugins: [
    new webpack.EnvironmentPlugin(['SPACE_ID']),
    new UglifyPlugin(),
    new HtmlPlugin({
      filename: 'index.html',
      template: './src/index.html',
      inject: false,
    }),
  ],
  resolve: {
    alias: {
      'react': 'preact-compat',
      'react-dom': 'preact-compat',
    },
  },
};
