const fs = require('fs');
const path = require('path');
const cli = require('contentful-extension-cli');

const client = cli.createClient({
  accessToken: process.env.CONTENTFUL_MANAGEMENT_ACCESS_TOKEN,
  spaceId: process.env.SPACE_ID,
});

client.getAll()
  .then((extensions) => {
    console.log(`Found ${extensions.items.length} extensions in the space id=${process.env.SPACE_ID}...`);

    const targetExtension = extensions.items.find(e => e.sys.id === 'flGlobalId');
    const nextVersion = targetExtension ? targetExtension.sys.version : 0;

    client.save({
      id: 'flGlobalId',
      name: 'Cross-space global content item ID',
      srcdoc: fs.readFileSync(path.resolve('dist/index.html')).toString(),
      fieldTypes: ['Symbol', 'Text'],
      version: nextVersion,
    })
      .then((savedExtension) => {
        console.log('Saved the extension.');
        console.log(savedExtension.sys);
      })
      .catch(e => {
        console.log('Could not save the extensions:', e);
      });
  })
  .catch(e => {
    console.log(`Could not get all extensions from the space ${process.env.SPACE_ID}:`, e.error.details.errors);
  });
