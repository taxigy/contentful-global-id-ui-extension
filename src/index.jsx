import React, { Component } from 'react';
import { render } from 'react-dom';
import { init } from 'contentful-ui-extensions-sdk';
import { getClients, getSpaceLanguageCode, getLatestIdInSpace } from './spaces';
import Input from './components/Input.jsx';

const MAX_ID_LENGTH = 2 + 5; // 2 for language code, 5 for the unique ID; all numbers.

console.log('Hello from Cross-Space ID UI extension.');

class Field extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      fieldValue: '',
    };

    this.setFieldValue = this.setFieldValue.bind(this);
  }

  async componentDidMount() {
    await getClients();

    let initValue = this.props.extension.field.getValue();

    if (!initValue) {
      const languageCode = getSpaceLanguageCode(process.env.SPACE_ID);
      const latestId = await getLatestIdInSpace(
        process.env.SPACE_ID,
        this.props.extension.field.id,
        this.props.extension.contentType.sys.id,
      );
      const nextId = latestId ? String(Number(latestId.slice(2)) + 1).padStart(MAX_ID_LENGTH - 2, '0') : '00000';

      initValue = `${languageCode}${nextId}`;
    }

    this.setFieldValue(initValue);
  }

  setFieldValue(value) {
    let nextValue = value;

    nextValue = nextValue.replace(/\D/g, ''); // remove all non-digit characters.

    if (nextValue.length > MAX_ID_LENGTH) {
      nextValue = nextValue.slice(0, MAX_ID_LENGTH);
    }

    this.setState({ fieldValue: nextValue });
    this.props.extension.field.setValue(nextValue);
  }

  render() {
    return (
      <div>
        <Input
          className="cf-form-input"
          value={this.state.fieldValue}
          onChange={event => this.setFieldValue(event.target.value)}
        />
      </div>
    );
  }
}

init(extension => {
  render(<Field extension={extension} />, document.getElementById('root'));
});
