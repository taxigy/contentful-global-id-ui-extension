import { createClient } from 'contentful/dist/contentful.browser.min.js';
import spaces from '../../../../config/contentful/spaces.json';

// const spaces = {
//   'bananas/en': {
//     languageCode: '01',
//     id: 'su97paydsjgn',
//     deliveryKey: 'adc2649e20ab49dd6838568ff8b5065c5c4f6612a2ad7b838ae1d36b8fac3f28',
//   },
// };

const clients = {};

function findSpaceById(spaceId) {
  if (!spaces || !spaces.constructor) {
    throw new Error('Cannot find a space by id because there are no spaces.');
  }

  if (spaces.constructor === Array) {
    return spaces.find(e => e.id === spaceId);
  }

  if (spaces.constructor === Object) {
    return Object.values(spaces).find(e => e.id === spaceId);
  }

  return {};
}

export function getClients() {
  if (Object.keys(clients).length === Object.keys(spaces).length) {
    return clients;
  }

  return Object.values(spaces).map((space) => {
    const client = createClient({
      space: process.env.SPACE_ID,
      accessToken: findSpaceById(process.env.SPACE_ID).deliveryKey,
    });

    clients[space.id] = client;

    return client;
  });
}

export function getSpaceLanguageCode(spaceId) {
  return findSpaceById(spaceId).languageCode;
}

export async function getLatestIdInSpace(spaceId, fieldId, contentTypeId) {
  if (!clients[spaceId]) {
    throw new Error('Cannot get the latest ID in space; call "getLatestIdInSpace" with Space ID as first argument.');
  }

  const response = await clients[spaceId].getEntries({
    order: `-fields.${fieldId}`,
    content_type: contentTypeId,
  });

  return response.items.length === 0 ? '0' : response.items[0].fields[fieldId];
}

// export function searchEntries(key, value) {
//   if (Object.keys(clients).length !== spaces.length) {
//     throw new Error('Initialize clients first; to do this, use "getClients" function.');
//   }
// }
