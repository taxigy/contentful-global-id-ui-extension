# Cross-space Global ID — Contentful UI Extension

## Credentials

Get the _space ID_. You can see it in the URL in browser:

```
https://app.contentful.com/spaces/ab12cd34ef56/entries
```

in this example, "ab12cd34ef56" is the space ID.

Next, you'll need _Content Management Token_. Go to <https://app.contentful.com/spaces/SPACE_ID/api/cma_tokens> (replace `SPACE_ID` with the real ID, it looks like "ab12cd34ef56") and create a CMA token there. Copy it and use it to deploy the UI extension.

Repeat all steps for every space.

## Environment variables

Build and deploy are both heavily impacted by environment variables. There are many, and all of them are equally important:

| Environment variable | Role in build | Role in deployment |
| `SPACE_ID` | Used by the UI extension on the run-time to detect the first two digits of the field value (which is meant to be the global ID for content). | Used by the contentful-extension-cli package to target deployment at a particular space. |
| `CONTENTFUL_MANAGEMENT_ACCESS_TOKEN` | _Not used_ | Used by the contentful-extension-cli package to authorize the PUT request to create or update a UI extension. |

## Build and deployment

Currently, deployment is implemented for a single space. It means that, in case of N spaces, you'll need to build and deploy N times. A better pipeline is TBD.

The workflow would then look like

```bash
SPACE_ID=ab12cd34ef56 yarn build
SPACE_ID=ab12cd34ef56 CONTENTFUL_MANAGEMENT_ACCESS_TOKEN=CFPAT-verylonghexstring yarn deploy
```

then go to the Contentful browser app, create a new content type and put the custom field at the topmost position.

## What works and what doesn't

TBD.
